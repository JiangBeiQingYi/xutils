package klg.db.utils;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import klg.common.utils.MyPrinter;
import klg.db.info.model.Column;
import klg.db.info.model.Table;

public class DBmetaTest {
	
	@Test
	public void getTablsTest() throws SQLException{
		List<Table> tables=DBmeta.getTabls("test");
		MyPrinter.print(tables);
		
	}
	@Test
	public void getColumnsTest() throws SQLException{
		List<Column> columns=DBmeta.getColumns("test", "user");
		MyPrinter.printJson(columns);
	}
	
}
