package klg.db.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.junit.Test;

import klg.common.utils.MyPrinter;
import klg.db.info.model.Column;

/**
 * apache dbutils 测试
 * @author kevin
 *
 */

public class DbUtilsTest {

	@Test
	public void toMap() throws SQLException {
		String sql = "select * from information_schema.`COLUMNS` WHERE TABLE_SCHEMA=\"test\"";
		List<Map<String, Object>> map = new QueryRunner().query(DBCPUtils.getConnection(), sql, new MapListHandler());
		MyPrinter.print(map);
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void toBean() throws SQLException {
		String sql = "select * from information_schema.`COLUMNS` WHERE TABLE_SCHEMA=\"test\" and TABLE_NAME=\"user\"";
		List<Column> columns = (List<Column>) new QueryRunner().query(DBCPUtils.getConnection(), sql, new BeanListHandler(Column.class));
		MyPrinter.print(columns);
	}
	@Test
	public void testConn() throws SQLException{
		String sql = "show databases";
		DBAccess dba = new DBAccess();

		dba.query(sql);
		ResultSet rs = dba.getRs();
		while (rs.next()) {
			System.out.println(rs.getString(1));
		}
	}
	
}
