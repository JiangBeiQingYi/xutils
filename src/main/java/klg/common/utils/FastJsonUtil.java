package klg.common.utils;

import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import com.alibaba.fastjson.util.TypeUtils;

/**
 * 使用fastjson 做输出，用jackson解析
 * 
 * @author kevin
 *
 */

public class FastJsonUtil {
	private static SerializeConfig config = new SerializeConfig();
	private static SerializerFeature[] features={
			SerializerFeature.PrettyFormat, // 格式化輸出
			SerializerFeature.DisableCircularReferenceDetect, // 禁止循环引用
			SerializerFeature.WriteMapNullValue // 输出null值
	};
	static {
		TypeUtils.compatibleWithJavaBean = true;// 禁止首字母小写
		config.put(Date.class, new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss"));
	}

	
	/**
	 * 将JavaBean序列化为带格式的JSON文本
	 * 
	 * @param object
	 * @param prettyFormat
	 * @return
	 */

	public static <T> String toJSONString(T obj) {
		return JSON.toJSONString(obj, config,features); 
	}

	/**
	 * 自定义时间格式
	 * @param dateFormat
	 * @param jsonText
	 * @return
	 */
	
	public static String toJSONString(String dateFormat, Object jsonText) {
		config.put(Date.class, new SimpleDateFormatSerializer(dateFormat));
		return JSON.toJSONString(jsonText, config,features);
	}
	
	
	
}