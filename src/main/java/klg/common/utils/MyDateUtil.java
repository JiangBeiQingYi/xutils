package klg.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MyDateUtil {

	public static final String DEF_DATE_FORMAT = "yyyy-MM-dd";
	public static final String DEF_TIME_FORMAT = "HH:mm:ss";
	public static final String DEF_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static Calendar calSetDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}

	public static int fullYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}

	public static Date parseDate(String dateStr, String format) {
		Date date = null;
		try {
			date = new SimpleDateFormat(format).parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static Date parseDateDefault(String dateStr) {
		return parseDate(dateStr, DEF_DATE_FORMAT);
	}

	public static Date parseDatetimeDefault(String date) {
		return parseDate(date, DEF_DATETIME_FORMAT);
	}

	public static int getWeekOfYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.WEEK_OF_YEAR);
	}

	private static int getWeekOfYear(String dateStr, String format) {
		return getWeekOfYear(parseDate(dateStr, format));
	}

	public static int getWeekOfYear(String dateStr) {
		return getWeekOfYear(parseDate(dateStr, DEF_DATE_FORMAT));
	}

	public static Date getNextWeekFirstDay() {
		Calendar cal = Calendar.getInstance();
		////在今天的基础上加一周
		cal.add(Calendar.WEEK_OF_YEAR, 1);
		//获取当前设置的一周第一天
		//每个星期的第一天根据locale不同而不同,一般默认1,周日
		int initDay = cal.getFirstDayOfWeek();
		System.out.println("initDay:"+initDay);
		//下周的第一天
		cal.set(Calendar.DAY_OF_WEEK, initDay);
		return cal.getTime();
	}

	public static Date getNextWeekLastDay() {
		Calendar cal = Calendar.getInstance();
		////在今天的基础上加一周
		cal.add(Calendar.WEEK_OF_YEAR, 1);
		//获取当前设置的一周第一天
		//每个星期的第一天根据locale不同而不同,一般默认1,周日
		int initDay = cal.getFirstDayOfWeek();
		//下周的最后一天
		cal.set(Calendar.DAY_OF_WEEK, initDay+6);
		return cal.getTime();
	}
	
	public static Date[] getWeekDays(int yyyy,int weekOfYear){
		Date[] weekDays=new Date[7];
		Calendar cal=Calendar.getInstance();
		//设置一年中的第几周
		cal.set(Calendar.YEAR, yyyy);
		cal.set(Calendar.WEEK_OF_YEAR, weekOfYear);
		for(int i=0;i<7;i++){
			//设置周几
			cal.set(Calendar.DAY_OF_WEEK, i+1);
			weekDays[i]=cal.getTime();
		}		
		return weekDays;
	}
	
	public static Date[] getWeekDays(Date date){
		Date[] weekDays=new Date[7];
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		for(int i=0;i<7;i++){
			//设置周几
			cal.set(Calendar.DAY_OF_WEEK, i+1);
			weekDays[i]=cal.getTime();
		}		
		return weekDays;
	}
	
	/**
	 * { "日", "一", "二", "三", "四", "五", "六" }
	 * 
	 * @param date
	 * @return
	 */
	public static String getCNDayofWeek(Date date) {
		String[] weekDays = { "日", "一", "二", "三", "四", "五", "六" };
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0)
			w = 0;
		return weekDays[w];
	}
	
	
	public static String getNextdayDateStrBy(String format) {
		return new SimpleDateFormat(format).format(getNextdayDateBy(format).getTime());
	}

	public static Date getNextdayDateBy(String format) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DATE, 1);
		return calendar.getTime();
	}

	public static String getDateStr(Date date) {
		return new SimpleDateFormat(DEF_DATE_FORMAT).format(date);
	}

	public static String getMonthStr(Date date) {
		return new SimpleDateFormat(DEF_DATE_FORMAT).format(date).substring(0, 7);
	}

	public static String getDatetimeStr(Date date) {
		return new SimpleDateFormat(DEF_DATETIME_FORMAT).format(date);
	}

	public static String getTimeStr(Date date) {
		return new SimpleDateFormat(DEF_TIME_FORMAT).format(date);
	}

	/**
	 * 字符串的日期格式的计算 相差天数
	 * 
	 * @param startDate
	 * @param endDate
	 * @return 相差天数 eg：daysBetween("2012-09-08", "2012-09-09") 返回1
	 * @throws ParseException
	 */
	public static int daysBetween(String startDate, String endDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(sdf.parse(startDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long time1 = cal.getTimeInMillis();

		try {
			cal.setTime(sdf.parse(endDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);
		return Integer.parseInt(String.valueOf(between_days));
	} 
}
